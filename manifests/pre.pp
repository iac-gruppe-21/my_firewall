class my_firewall::pre(
    $iptables_path = "/etc/iptables/iptables.v4",
) {
  Firewall {
    require => undef,
  }
file{ '/etc/iptables/rules.v4':
    path   => ($iptables_path),
    ensure => 'file',
}
  # Default firewall rules
  firewall { '000 accept all icmp':
    proto  => 'icmp',
    action => 'accept',
  }
  -> firewall { '001 accept all to lo interface':
    proto   => 'all',
    iniface => 'lo',
    action  => 'accept',
  }
  -> firewall { '002 reject local traffic not on loopback interface':
    iniface     => '! lo',
    proto       => 'all',
    destination => '127.0.0.1/8',
    action      => 'reject',
  }
  -> firewall { '003 accept related established rules':
    proto  => 'all',
    state  => ['RELATED', 'ESTABLISHED'],
    action => 'accept',
  }

  -> firewall {'004 reject traffic on this port':
    dport  => 135,
    proto  => ['tcp', 'udp'],
    action => 'reject',
  }
  -> firewall { '005 accept ssh on this machine':
    dport  => 22,
    proto  => 'tcp',
    action => 'accept',
  }
 -> firewall { '006 deny http traffic':
    dport  => 80,
    proto  => 'tcp',
    action => 'reject',
  }
class profile::apache {
  include apache
  apache::vhost { 'mysite':
    ensure => present,
  }

  firewall { '100 allow http and https access':
    dport  => [80, 443],
    proto  => 'tcp',
    action => 'reject',
  }
}

}
