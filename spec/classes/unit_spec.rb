require 'spec_helper'

describe 'my_firewall::pre', type: :class do 
  on_supported_os.each do |os, os_facts|
    context "on #{os} with default settings" do
      let(:facts) { os_facts }
      let(:node) {'node.consul.dir' }
			it {is_expected.to compile.with_all_deps }
      it { is_expected.to contain_file('/etc/iptables/rules.v4').with(:ensure => 'file') }
      # it {is_expected.to compile }
      #RSpec.configuremodule_path do |c|
      #  puts c
      #end 
      #  it {is_expected.to compile.and_raise_error(RSpec.configure.module_path) }
    end
  end
end

