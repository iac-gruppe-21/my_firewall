# frozen_string_literal: true

UNSUPPORTED_PLATFORMS = ['aix', 'windows', 'solaris'].freeze

def iptables_version
  result = run_shell('iptables -V')
  result.stdout.match(%r{\s(v\d{1,2}\.\d\.\d)})[1]
end

